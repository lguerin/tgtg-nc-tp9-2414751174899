import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'nc-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  email: FormControl;
  password: FormControl;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initLoginForm();
  }

  getErrorMessage(control: FormControl): string {
    let error = '';
    if (control.hasError('required')) {
      error = 'Champ obligatoire';
    }
    if (control.hasError('email')) {
      error = 'Email invalide';
    }
    return error;
  }

  /**
   * Initialise le formulaire de login
   */
  initLoginForm(): void {
    // Init des champs
    this.email = this.fb.control('', [Validators.required, Validators.email]);
    this.password = this.fb.control('', [Validators.required]);

    // Build du formulaire
    this.loginForm = this.fb.group({
      email: this.email,
      password: this.password
    });
  }

  /**
   * Authentifier un utilisateur
   */
  authenticate(): void {
    console.log('>> ', this.loginForm.getRawValue());
  }
}
